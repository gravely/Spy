Spy
===

An OSX script wrapped in an app that just snags a full screen screenshot, thumbnails it, scps both, and fires a Mountain Lion notification center alert.

Prerequisites
===

You'll want [terminal-notifier.app](https://github.com/alloy/terminal-notifier "Terminal Notifier") for the Mountain Lion notifcaiton center integration.

Configuration
===

Everything is in Spy.app/Contents/Resources/script, which is a simple shell script that must be configured prior to use.

